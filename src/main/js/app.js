'use strict';

import {BrowserRouter} from 'react-router-dom';
/*import {Header} from "./header";*/
import {Main} from "./main";

// tag::vars[]
const React = require('react');
const ReactDOM = require('react-dom');

// end::vars[]

const App = () => (
    <div>
        {/*<Header />*/}
        <Main />
    </div>
);

// tag::render[]
ReactDOM.render(
    <BrowserRouter>
        <App/>
    </BrowserRouter>,
    document.getElementById('react')
);
// end::render[]