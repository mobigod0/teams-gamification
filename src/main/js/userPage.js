const React = require('react');
const createReactClass = require('create-react-class');

var UserPage = createReactClass({

    getInitialState: function () {
        return {
            skills: [],
            teams: [],
            wiw: '',
            id: ''
        }
    },

    componentWillMount: function () {
        const wiw = this.props.match.params.wiw;

        fetch('/user/' + wiw, {
            method: 'POST',
            credentials: 'include'
        }).then(function (response) {
            if (response.status === 422) {
                console.log("There is no user with wiw ", wiw);
            } else if (response.status === 200) {
                return response.json();
            }
        }).then(function (json) {
            console.log(json);
            this.setState({
                skills: json.skills,
                teams: json.teams,
                wiw: wiw,
                id: json.id
            });
        }.bind(this));
    },

    render: function () {
        return (
            <div>
                Page of user {this.state.wiw}

                <div>
                    Skills:
                    <table>
                        <thead>
                        <tr key='42893hr2frygfvdg34r'>
                            <td>Id</td>
                            <td>Name</td>
                            <td>Value</td>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.skills.map(skill => {
                                return (
                                    <tr key={skill.id}>
                                        <td>{skill.id}</td>
                                        <td>{skill.name}</td>
                                        <td>{skill.value}</td>
                                    </tr>
                                );
                            })
                        }
                        </tbody>
                    </table>
                </div>

                <div>
                    Teams:
                    {
                        this.state.teams.map(team => {
                            return (
                                <div>
                                    Team name {team.name} <br/>
                                    Team id {team.id} <br/>
                                    Team clients:
                                    <table>
                                        <thead>
                                        <tr key='590235923498hig'>
                                            <td>Id</td>
                                            <td>Wiw</td>
                                            <td>Link</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            team.clients.map(client => {
                                                return (
                                                    <tr key={client.id}>
                                                        <td>{client.id}</td>
                                                        <td>{client.wiw}</td>
                                                        <td><a href={location.origin + '/user/' + client.wiw}>Link</a></td>
                                                    </tr>
                                                );
                                            })
                                        }
                                        </tbody>
                                    </table>
                                </div>
                            );
                        })
                    }
                </div>
            </div>
        );
    }
});

module.exports = UserPage;
