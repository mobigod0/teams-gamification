package com.team.gamification.controller.interfaces;

import org.springframework.web.bind.annotation.GetMapping;

public interface LoginController {
    /**
     * Spring security method
     */
    @GetMapping("/login")
    String login();
}
