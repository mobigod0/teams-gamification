package com.team.gamification.controller.interfaces;

import com.team.gamification.dto.GetTeamRequest;
import com.team.gamification.dto.TeamDto;
import com.team.gamification.dto.GetTeamByUserRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

public interface TeamContoller {
    @ResponseBody
    @PostMapping("/team")
    ResponseEntity<List<TeamDto>> getTeam(@RequestBody GetTeamRequest request);

    @ResponseBody
    @PostMapping("/team/user")
    ResponseEntity<List<TeamDto>> getTeam(@RequestBody GetTeamByUserRequest request);
}
