package com.team.gamification.controller.impl;

import com.team.gamification.controller.interfaces.LoginController;
import org.springframework.stereotype.Controller;

@Controller
public class LoginControllerImpl implements LoginController {
    @Override
    public String login() {
        return "/index.html";
    }
}
