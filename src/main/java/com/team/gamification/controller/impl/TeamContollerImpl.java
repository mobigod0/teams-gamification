package com.team.gamification.controller.impl;

import com.team.gamification.controller.interfaces.TeamContoller;
import com.team.gamification.dto.GetTeamRequest;
import com.team.gamification.dto.TeamDto;
import com.team.gamification.dto.GetTeamByUserRequest;
import com.team.gamification.dto.TeamRequest;
import com.team.gamification.service.interfaces.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TeamContollerImpl implements TeamContoller {
    private final TeamService teamService;

    @Autowired
    public TeamContollerImpl(TeamService teamService) {
        this.teamService = teamService;
    }

    @Override
    @ResponseBody
    public ResponseEntity<List<TeamDto>> getTeam(@RequestBody GetTeamRequest request) {
        return teamService.getTeam(new TeamRequest(request.getRequesterUserId(), request.getTeamId(), null));
    }

    @Override
    @ResponseBody
    public ResponseEntity<List<TeamDto>> getTeam(@RequestBody GetTeamByUserRequest request) {
        return teamService.getTeam(new TeamRequest(request.getRequesterUserId(), null, request.getUserId()));
    }
}
