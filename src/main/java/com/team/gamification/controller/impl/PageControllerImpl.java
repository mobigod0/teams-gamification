package com.team.gamification.controller.impl;

import com.team.gamification.controller.interfaces.PageController;
import org.springframework.stereotype.Controller;

@Controller
public class PageControllerImpl implements PageController {

    @Override
    public String page() {
        return "/index.html";
    }
}
