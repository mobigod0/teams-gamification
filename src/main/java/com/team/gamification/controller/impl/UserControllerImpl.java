package com.team.gamification.controller.impl;

import com.team.gamification.controller.interfaces.UserController;
import com.team.gamification.dto.ClientDto;
import com.team.gamification.dto.UserRequest;
import com.team.gamification.service.interfaces.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;

@Controller
public class UserControllerImpl implements UserController {
    private final ClientService clientService;

    @Autowired
    public UserControllerImpl(ClientService clientService) {
        this.clientService = clientService;
    }

    @Override
    @ResponseBody
    public ResponseEntity<ClientDto> getUser(Principal principal, @PathVariable String wiw) {
        return clientService.getUser(new UserRequest(wiw, principal.getName()));
    }

    @Override
    public String getUserPage(@PathVariable String wiw) {
        return "/index.html";
    }
}
