package com.team.gamification.security;

import com.team.gamification.model.Client;
import com.team.gamification.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClientLoginSecurity implements UserDetailsService {

    private final ClientRepository clientRepository;

    @Autowired
    public ClientLoginSecurity(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String wiw) throws UsernameNotFoundException {
        Optional<Client> clientOptional = clientRepository.findClientByWiw(wiw);
        if (!clientOptional.isPresent()) {
            throw new UsernameNotFoundException("User " + wiw + " not found.");
        }

        Client client = clientOptional.get();
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        client.getRoles().forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getName())));
        return new User(client.getWiw(), client.getPassword(), authorities);
    }
}
