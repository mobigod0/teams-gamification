package com.team.gamification.service.interfaces;

import com.team.gamification.dto.TeamDto;
import com.team.gamification.dto.TeamRequest;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TeamService {
    ResponseEntity<List<TeamDto>> getTeam(TeamRequest teamRequest);
}
