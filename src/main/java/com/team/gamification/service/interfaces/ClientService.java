package com.team.gamification.service.interfaces;

import com.team.gamification.dto.ClientDto;
import com.team.gamification.dto.UserRequest;
import org.springframework.http.ResponseEntity;

public interface ClientService {
    ResponseEntity<ClientDto> getUser(UserRequest userRequest);
}
