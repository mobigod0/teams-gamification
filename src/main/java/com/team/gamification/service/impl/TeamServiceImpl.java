package com.team.gamification.service.impl;

import com.team.gamification.converter.TeamConverter;
import com.team.gamification.dto.TeamDto;
import com.team.gamification.dto.TeamRequest;
import com.team.gamification.model.Client;
import com.team.gamification.model.Team;
import com.team.gamification.repository.ClientRepository;
import com.team.gamification.repository.TeamRepository;
import com.team.gamification.service.interfaces.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TeamServiceImpl implements TeamService {
    private TeamRepository teamRepository;
    private ClientRepository clientRepository;
    private TeamConverter teamConverter;

    @Autowired
    public TeamServiceImpl(TeamRepository teamRepository, ClientRepository clientRepository, TeamConverter teamConverter) {
        this.teamRepository = teamRepository;
        this.clientRepository = clientRepository;
        this.teamConverter = teamConverter;
    }

    @Override
    public ResponseEntity<List<TeamDto>> getTeam(TeamRequest teamRequest) {

        Optional<Client> requesterClientOptional = clientRepository.findById(teamRequest.getRequesterUserId());
        if(!requesterClientOptional.isPresent()) {
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        if (teamRequest.getTeamId() != null) {
            Optional<Team> teamOptional = teamRepository.findById(teamRequest.getTeamId());
            return teamOptional
                    .map(team -> new ResponseEntity<>(
                            Collections.singletonList(
                                    teamConverter.toTeamDto(team, requesterClientOptional.get())), HttpStatus.OK))
                    .orElseGet(() -> new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY));
        } else {
            Optional<Client> teamClientOptional = clientRepository.findById(teamRequest.getTeamUserId());

            if (!teamClientOptional.isPresent()) {
                return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
            }
            List<Team> teams = teamRepository.findTeamsByClientsContains(Collections.singletonList(teamClientOptional.get()));
            return new ResponseEntity<>(
                    teams.stream()
                            .map(team -> teamConverter.toTeamDto(team, requesterClientOptional.get()))
                            .collect(Collectors.toList()),
                    HttpStatus.OK);
        }
    }
}
