package com.team.gamification.service.impl;

import com.team.gamification.converter.ClientConverter;
import com.team.gamification.dto.ClientDto;
import com.team.gamification.dto.UserRequest;
import com.team.gamification.model.Client;
import com.team.gamification.repository.ClientRepository;
import com.team.gamification.service.interfaces.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClientServiceImpl implements ClientService {
    private ClientRepository clientRepository;
    private ClientConverter clientConverter;

    @Autowired
    public ClientServiceImpl(ClientRepository clientRepository, ClientConverter clientConverter) {
        this.clientRepository = clientRepository;
        this.clientConverter = clientConverter;
    }

    @Override
    public ResponseEntity<ClientDto> getUser(UserRequest userRequest) {
        Optional<Client> clientOptional = clientRepository.findClientByWiw(userRequest.getWiw());

        return clientOptional.map(client -> new ResponseEntity<>(clientConverter.toClientDto(client,
                userRequest.getRequesterWiw().equals(userRequest.getWiw())), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY));
    }
}
