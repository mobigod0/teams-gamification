package com.team.gamification.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class TeamDto {
    private Long id;
    private String name;
    private List<ClientDto> clients;
}
