package com.team.gamification.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ClientDto {
    private Long id;
    private String wiw;
    private List<SkillDto> skills;
    private List<TeamDto> teams;
}
