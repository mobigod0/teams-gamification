package com.team.gamification.converter;

import com.team.gamification.dto.ClientDto;
import com.team.gamification.model.Client;
import com.team.gamification.model.ClientSkill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.stream.Collectors;

@Component
public class ClientConverter {
    private final TeamConverter teamConverter;
    private final SkillConverter skillConverter;

    @Autowired
    public ClientConverter(TeamConverter teamConverter, SkillConverter skillConverter) {
        this.teamConverter = teamConverter;
        this.skillConverter = skillConverter;
    }

    public ClientDto toClientDto(Client client, boolean filterSkills) {
        return ClientDto.builder()
                .id(client.getId())
                .wiw(client.getWiw())
                .skills(client.getClientSkills().stream()
                        .map(ClientSkill::getSkill)
                        .filter(skill ->
                                (!filterSkills || skill.getRoles().isEmpty()) ||
                                        !Collections.disjoint(skill.getRoles(), client.getRoles())
                        )
                        .map(skill -> skillConverter.toSkillDto(skill, client))
                        .collect(Collectors.toList()))
                .teams(client.getTeams().stream().map(team -> teamConverter.toTeamDto(team, client)).collect(Collectors.toList()))
                .build();
    }
}
