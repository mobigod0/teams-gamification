package com.team.gamification.converter;

import com.team.gamification.dto.SkillDto;
import com.team.gamification.model.Client;
import com.team.gamification.model.ClientSkill;
import com.team.gamification.model.Skill;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class SkillConverter {
    public SkillDto toSkillDto(Skill skill, Client client) {
        Optional<ClientSkill> clientSkillOptional = client.getClientSkills().stream()
                .filter(skill1 -> skill1.getSkill().getId().equals(skill.getId()))
                .findFirst();

        return clientSkillOptional.map(clientSkill -> SkillDto.builder()
                .id(skill.getId())
                .name(skill.getName())
                .value(clientSkill.getValue())
                .build())
                .orElse(null);
    }
}
